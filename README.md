# Anis Services 

## Introduction

AstroNomical Information System is a generic web tool that aims to facilitate the provision of data (Astrophysics), accessible from a database, for the scientific community.

This software allows you to control one or more databases related to astronomical projects and allows access to datasets via URLs.

This repository is the `anis-services` sub-project.

Anis is protected by the CeCILL licence (see LICENCE file at the software root).

## Authors

Here is the list of people involved in the development:

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Chrystel Moreau` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille (CNRS)

## More resources:

* [Website](https://anis.lam.fr)
* [Documentation](https://anis.lam.fr/doc/)

## Installing and starting the application

Anis Services contains a Makefile that helps the developer to install and start the application.

To list all operations availables just type `make` in your terminal at the root of this application.

- To install all dependancies into a docker image : make install
- To start/stop/restart/status all services : `make start|stop|restart|status`
- To display logs for all services : `make logs`
- To open a shell command into anis-services container : `make shell`

## Application

After the start application is available at: [http://localhost:5000](http://localhost:5000)
