UID := 1000
GID := 1000
NAME_APP=anis-services
#DOCKER_HOST=172.17.0.1
DOCKER_HOST=host.docker.internal

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  install        > build $(NAME_APP) docker image"
	@echo "  start          > start containers"
	@echo "  restart        > restart containers"
	@echo "  stop           > stop and kill running containers"
	@echo "  status         > display stack containers status"
	@echo "  logs           > display containers logs"
	@echo "  shell          > shell into $(NAME_APP) container"
	@echo ""

install:
	@docker build -f ./Dockerfile.dev -t anis-services . 

start:
	@docker run --init -it --rm \
	--name $(NAME_APP) \
	-e FLASK_ENV=development \
	-e DATA_PATH=/data \
	-e SERVER_URL=http://$(DOCKER_HOST):8080 \
	-p 5000:5000 \
	-v $(CURDIR)/src:/usr/src/app \
	-v $(CURDIR)/data:/data -d \
	anis-services
#sleep infinity

restart: stop start

stop:
	@docker stop $(NAME_APP)

status:
	@docker ps -f name=$(NAME_APP)

logs:
	@docker logs -f -t $(NAME_APP)

shell:
	@docker exec -ti $(NAME_APP) bash
