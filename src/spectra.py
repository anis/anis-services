# Third party imports
from astropy.io import fits

# Local application imports
from spectra_strategies import StrategySolver
from spectra_strategies import DefaultStrategy
from spectra_strategies import SixdFStrategy
from spectra_strategies import GamaDR2AATStrategy
from spectra_strategies import GamaDR2LTStrategy
from spectra_strategies import ZCosmosBrightDr3Strategy

def spectra_to_csv(filename):
    hdulist = fits.open(filename)

    if(filename.count('6dF') > 0):
        solver = StrategySolver(SixdFStrategy())
    elif(filename.count('XXL-AAOmega') > 0):
        solver = StrategySolver(DefaultStrategy())
    elif(filename.count('AAT') > 0):
        solver = StrategySolver(GamaDR2AATStrategy())
    elif(filename.count('LT') > 0):
        solver = StrategySolver(GamaDR2LTStrategy())
    elif(filename.count('zCOSMOS_BRIGHT_DR3') > 0):
        solver = StrategySolver(ZCosmosBrightDr3Strategy())
    else:
        solver = StrategySolver(DefaultStrategy())

    csv = solver.execute(hdulist)
    hdulist.close()
    return csv