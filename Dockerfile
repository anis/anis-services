FROM python:3.8

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY src/ ./
RUN pip install numpy
RUN pip install --no-cache-dir -r requirements.txt

CMD ["python3.8", "app.py"]
